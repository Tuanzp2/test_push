/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sopvn.repositories;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import sopvn.model.Category;
import sopvn.util.DBConnection;

/**
 *
 * @author khiemle
 */
public class CategoryRepository {
    
    /***
     * get all category
     */
    public HashSet<Category> findAll() {
        Statement stmt = null;
        ResultSet rs = null;
        HashSet<Category> ls=new HashSet<Category>();
        Connection conn=DBConnection.Instance().DBConnect();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM "+Category.CATE_TBL );         
            // Now do something with the ResultSet ....
            while (rs.next()) {                
                Category item=new Category();
                item.setId(rs.getInt(Category.ID_COL));
                item.setName(rs.getString(Category.Name_COL));
                item.setActive(rs.getInt(Category.Active_COL));
                ls.add(item);
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
      
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException sqlEx) {
                } // ignore
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                } // ignore

                stmt = null;
            }
        }
        return ls;
    }
}
