/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package sopvn.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author khiemle
 */
public class ProductMgrController implements Initializable {

    @FXML
    TextField tfPro_name;
    @FXML
    TextField tfPro_price;
    @FXML
    ComboBox cboCategories;//get ID category
    @FXML
    GridPane grProduct;
    @FXML
    Button btnSave;
    @FXML
    Button btnReset;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        btnSave.addEventHandler(ActionEvent.ACTION, (eh) -> {

        });
        btnReset.addEventHandler(ActionEvent.ACTION, (eh) -> {

        });
    }    
    
}
