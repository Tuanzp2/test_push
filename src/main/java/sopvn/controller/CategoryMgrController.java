/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package sopvn.controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 * FXML Controller class
 *
 * @author khiemle
 */
public class CategoryMgrController implements Initializable {

    @FXML
    TextField tfCate_name;
    @FXML
    CheckBox ckCate_active;

    @FXML
    Button btnSave;
    @FXML
    Button btnReset;
    
    @FXML
    GridPane grCategories;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        btnSave.addEventHandler(ActionEvent.ACTION, (eh) -> {

        });
        btnReset.addEventHandler(ActionEvent.ACTION, (eh) -> {

        });
    }

}
