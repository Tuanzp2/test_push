/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sopvn.model;

/**
 *
 * @author khiemle
 */
public class Category {

   

    private int _id;
    private String _name;
    private int _active;
     public static String CATE_TBL = "category";
    public static String ID_COL = "idcategory";
    public static String Name_COL = "category_name";
    public static String Active_COL = "category_active";

    public Category() {
    }

    public Category(int _id, String _name, int _active) {
        this._id = _id;
        this._name = _name;
        this._active = _active;
    }
    
    
     /**
     * @return the _id
     */
    public int getId() {
        return _id;
    }

    /**
     * @param _id the _id to set
     */
    public void setId(int _id) {
        this._id = _id;
    }

    /**
     * @return the _name
     */
    public String getName() {
        return _name;
    }

    /**
     * @param _name the _name to set
     */
    public void setName(String _name) {
        this._name = _name;
    }

    /**
     * @return the _active
     */
    public int getActive() {
        return _active;
    }

    /**
     * @param _active the _active to set
     */
    public void setActive(int _active) {
        this._active = _active;
    }
}
