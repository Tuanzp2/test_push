/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sopvn.modelview;

import sopvn.model.*;

/**
 *
 * @author khiemle
 */
public class ProductView {

    private int _id;
    private String _name;
    private double _price;
    private int _idCate;
    private String _cate_name;
    public static String PRO_TBL = "product";
    public static String ID_COL = "id_pro";
    public static String NAME_COL = "name_pro";
    public static String PRICE_COL = "price";
    public static String CATE_ID_COL = "id_cate";

    /**
     * @return the _id
     */
    public int getId() {
        return _id;
    }

    /**
     * @param _id the _id to set
     */
    public void setId(int _id) {
        this._id = _id;
    }

    /**
     * @return the _name
     */
    public String getName() {
        return _name;
    }

    /**
     * @param _name the _name to set
     */
    public void setName(String _name) {
        this._name = _name;
    }

    /**
     * @return the _price
     */
    public double getPrice() {
        return _price;
    }

    /**
     * @param _price the _price to set
     */
    public void setPrice(double _price) {
        this._price = _price;
    }

    /**
     * @return the _idCate
     */
    public int getIdCate() {
        return _idCate;
    }

    /**
     * @param _idCate the _idCate to set
     */
    public void setIdCate(int _idCate) {
        this._idCate = _idCate;
    }

    public void setCate_name(String _cate_name) {
        this._cate_name = _cate_name;
    }

    public String getCate_name() {
        return _cate_name;
    }

    public ProductView() {
    }

    public ProductView(int _id, String _name, double _price, int _idCate) {
        this._id = _id;
        this._name = _name;
        this._price = _price;
        this._idCate = _idCate;
    }

}
