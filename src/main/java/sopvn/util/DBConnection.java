/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sopvn.util;

import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.SQLException;


/**
 *
 * 
 * @author khiemle
 */
public final class DBConnection {

    private String connection_url = "jdbc:mysql://localhost:3306/demo_flutter?user=flutter1&password=123";
    
    private static DBConnection instance=null;
    
    //constructor private
    private DBConnection(){
    
    }
    public static DBConnection Instance(){
        if (instance==null) {
            instance =new DBConnection();
        }
        return instance;
    }
    public Connection DBConnect() {
        try {
//            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//            conn = DriverManager.getConnection(_url, "demousr", "123");
            Connection conn = null;
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(connection_url);
            return conn;
        } catch (ClassNotFoundException ex) {
            
        } catch (SQLException ex) {
            
        }
        return null;
    }
}
