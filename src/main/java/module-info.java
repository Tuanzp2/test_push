module sopvn {
    requires javafx.controls;
    requires javafx.fxml;
    
    requires java.base;
    requires com.microsoft.sqlserver.jdbc;
    requires java.sql;
    opens sopvn to javafx.fxml;
    exports sopvn;
    exports sopvn.controller;
    
   
}
